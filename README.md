# git-import

Similar to `go-import`, a way to create vanity URLs for git repository paths.

Information on `go-import` can be found on the [golang website](https://golang.org/cmd/go/#hdr-Remote_import_paths)

`git-import` strives to work in a similar manner  
By providing a `meta` tag with appropriate information, `git-import` will clone the specified repository

The following is the `meta` tag for this repository, hosted on https://go.jolheiser.com/git-import with [Vanity](https://gitea.com/jolheiser/vanity)

```html
<meta name="git-import" content="git-import https://gitea.com/jolheiser/git-import.git git@gitea.com:jolheiser/git-import.git" />
```


## SSH
`git-import` can set up SSH if applicable, however it must be ran with `GIT_SSH_COMMAND` set in order to configure the repository properly.

## Examples

Clone this repository  
`git-import go.jolheiser.com/git-import`

Clone this repository with SSH  
`GIT_SSH_COMMAND="/usr/bin/ssh -i /home/user/.ssh/id_rsa" git-import -s go.jolheiser.com/git-import`

Clone this repository, but clone into "import-git"  
`git-import go.jolheiser.com/git-import import-git`

Output the repository URL of this repo (without cloning)  
`git-import -d go.jolheiser.com/git-import`

Output the repository SSH URL of this repo (without cloning)  
`git-import -d -s go.jolheiser.com/git-import`

## Bonus Points

Create a git alias for `git-import` 
```
git config --global alias.get '!/path/to/git-import'
```
and use like `git get go.jolheiser.com/git-import`