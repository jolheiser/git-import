package main

import (
	"fmt"
	"html/template"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var (
	name      = "repo"
	cloneHTTP = "http://www.git.com/user/repo.git"
	cloneSSH  = "git@git.com:user/repo.git"

	tpl1 = `<html><head><meta name="git-import" content="repo http://www.git.com/user/repo.git" /></head><body></body></html>`
	tpl2 = `<html><head><meta name="git-import" content="repo http://www.git.com/user/repo.git git@git.com:user/repo.git" /></head><body></body></html>`
	tpl3 = `<html><head><meta name="git-import" content="repo git@git.com:user/repo.git" /></head><body></body></html>`
	tpl4 = `<html><head><meta name="git-import" content="repo http://www.git.com/user/repo.git http://www.git.com/user/repo.git" /></head><body></body></html>`
	h1   = handle1{}
	h2   = handle2{}
	h3   = handle3{}
	h4   = handle4{}
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestGitImport(t *testing.T) {

	tt := []struct {
		name    string
		handler http.Handler
		ssh     bool
		value   string
		err     bool
	}{
		{name: "HTTPS 1", handler: h1, value: cloneHTTP},
		{name: "HTTPS 2", handler: h2, value: cloneHTTP},
		{name: "HTTPS 3", handler: h3, err: true},

		{name: "SSH 1", handler: h1, ssh: true, value: ""},
		{name: "SSH 2", handler: h2, ssh: true, value: cloneSSH},
		{name: "SSH 4", handler: h4, ssh: true, err: true},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, "", nil)
			tc.handler.ServeHTTP(rec, req)
			gi, err := parseMetaGitImport(rec.Body)
			if err != nil {
				if tc.err {
					return
				}
				t.Log(err)
				t.FailNow()
			}
			if tc.err {
				format := "GitImport.Name: %s\nGitImport.HTTP:        %s\nGitImport.SSH:         %s"
				formatted := fmt.Sprintf(format, gi.Name, gi.HTTP, gi.SSH)
				t.Logf("test-case should have produced an error\n%s", formatted)
				t.FailNow()
			}
			if gi.Name != name {
				expected(t, name, gi.Name)
			}
			if !tc.ssh && gi.HTTP != tc.value {
				expected(t, tc.value, gi.HTTP)
				t.FailNow()
			}
			if tc.ssh && gi.SSH != tc.value {
				expected(t, tc.value, gi.SSH)
				t.FailNow()
			}
		})
	}
}

//   _   _      _
//  | | | | ___| |_ __   ___ _ __ ___
//  | |_| |/ _ \ | '_ \ / _ \ '__/ __|
//  |  _  |  __/ | |_) |  __/ |  \__ \
//  |_| |_|\___|_| .__/ \___|_|  |___/
//               |_|

func expected(t *testing.T, expected, got string) {
	t.Logf("\nexpected: %s\n     got: %s", expected, got)
}

func serve(tplName, tpl string, res http.ResponseWriter) {
	tmpl, err := template.New(tplName).Parse(tpl)
	if err != nil {
		fmt.Printf("could not parse template: %v", err)
	}
	if err := tmpl.Execute(res, nil); err != nil {
		fmt.Printf("could not execute template: %v", err)
	}
}

type handle1 struct{}

func (h1 handle1) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	serve("tpl1", tpl1, res)
}

type handle2 struct{}

func (h2 handle2) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	serve("tpl2", tpl2, res)
}

type handle3 struct{}

func (h3 handle3) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	serve("tpl3", tpl3, res)
}

type handle4 struct{}

func (h4 handle4) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	serve("tpl4", tpl4, res)
}
