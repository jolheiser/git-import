module go.jolheiser.com/git-import

go 1.12

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
)
